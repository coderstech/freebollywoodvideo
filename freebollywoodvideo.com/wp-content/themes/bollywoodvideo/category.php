<?php

get_header(); ?>



  <div class="col-xs-12 col-sm-12 col-md-12">	
		<div id="search-song">
			<div class="title-main">
		      <span class="title-b "><?php printf('%s', single_cat_title( '', false ) ); ?></span>
			   
			</div>
		</div>	
		<div class="clearfix"></div>
		<div class="song-detail-blok">
	          <div class="col-xs-12 col-sm-12 col-md-12">  
				<div class="row">				
					 
					
					<?php $loop = new WP_Query( array( 'post_type' => 'videos','meta_key'			=> 'isvalid',
					'orderby'			=> 'meta_value_num',
					'order'				=> 'DESC', 'category_name' => single_cat_title( '', false ) ) ); ?>

					<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
					
					  
					
						<div class="col-xs-12 col-ms-4 col-sm-3 col-md-3 ">
						 
				 
								<div class="music-thumb">
									<a class="mainimg" href="<?php echo get_permalink() ?>"><?php echo get_the_post_thumbnail( get_the_ID(), array( 300, 152) );	?></a>
									<!--<p class="duration">2:38</p>-->
									<a href="<?php echo get_permalink() ?>" title="<?php echo get_the_title() ?>" class="artwork_play"> <img src="<?php bloginfo('template_url'); ?>/images/play-ic.PNG" class="play"></a>
								</div>
								<div class="song-title">
									<div class="visible-ms visible-sm visible-md visible-lg"><div class="pull-left" style="margin: 0px;"><?php if(function_exists('the_ratings')) { the_ratings(); } ?></div><div class="pull-right" style="margin: 0px;"><p>Views : <?php echo get_post_meta(get_the_ID(),'total_view',true); ?> </p></div><div class="clearfix"></div></div>									
									  <h4><?php echo get_the_title() ?></h4>
									  <div class="visible-xs"><?php if(function_exists('the_ratings')) { the_ratings(); } ?><p  style="margin: 8px 0;">Views : <?php echo get_post_meta(get_the_ID(),'total_view',true); ?> </p><div class="clearfix"></div></div>									
										 
										
								</div>
						</div>
							     			
					
					
					<?php endwhile;  ?>
				 </div>
			   </div>	
		 </div>		
				
				
		
	 
  </div>	  
 	 

<?php get_footer(); ?>

	
