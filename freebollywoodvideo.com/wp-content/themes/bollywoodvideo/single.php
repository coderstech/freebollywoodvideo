<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress 
 */
get_header(); 

 

?>

<div class="col-xs-12 col-sm-12 col-md-12">	
		<div id="search-song">
			<div class="title-main">
		      <span class="title-b "><?php the_title() ?></span>
			   
			</div>
		</div>	
		<?php while ( have_posts() ) : the_post(); 
		       $vidfile=get_post_meta(get_the_ID(),'video_url',true);
		       $isvalid=get_post_meta(get_the_ID(),'isvalid',true);
		       if($isvalid==1){
					$videochek=file_get_contents($vidfile); 
					if( $videochek) {
						$isvideo=true;
					}else{
						$isvideo=false;
						update_post_meta(get_the_ID(),'isvalid','0');
						
					}	
				}else{
					$isvideo=false;
				}
			   update_post_meta(get_the_ID(),'total_view',intval(get_post_meta(get_the_ID(),'total_view',true))+1);	
		       $post_thumbnail_id = get_post_thumbnail_id( get_the_ID() );
			   $image_attributes = wp_get_attachment_image_src( $post_thumbnail_id ); // returns an array
		
		 ?>
		<div class="clearfix"></div>
		<div class="song-detail-blok" style="border-bottom:0px">
	          <div class="col-xs-12 col-sm-12 col-md-12 text-center">  
 
					<?php if($isvideo){?>
						   <div id="jwplayerload">Loading the player...</div>
					<?php }else{  
						
						echo get_the_post_thumbnail( get_the_ID(),array(450,300));
						
					  } ?>	
					

				 
	          </div>
	          <div class="col-xs-12 col-sm-12 col-md-12" style="margin-top:20px;font-weight:bold">  
	          <div class="col-xs-12 col-ms-6 ">
				  
				  <a href="javascript:void(0)" onclick="downfile('<?php echo get_permalink(83) ?>?downloads=<?php echo base64_encode(get_the_ID()) ?>')"><img src="<?php echo site_url()?>/wp-content/themes/bollywoodvideo/images/download.png" /> Download</a>
				  
				  <div class="clearfix" style="margin-top:10px"></div>
				  <?php if(function_exists('the_ratings')) { the_ratings(); } ?> 
				  
				   
	          </div>
	          <div class="col-xs-12 col-ms-6 text-right  visible-ms visible-md visible-lg visible-sm">
				 <p  style="margin: 8px 0;">Views : <?php echo get_post_meta(get_the_ID(),'total_view',true); ?> </p>
				  <div class="clearfix" style="margin-top:10px"></div>
				  <div>
				  Share on : 
				 <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u="><img src="<?php echo site_url()?>/wp-content/themes/bollywoodvideo/css/images/facebook_32.png"  title="Share on facebook"/></a>
				 
				 <a target="_blank" href="https://twitter.com/home?status="><img src="<?php echo site_url()?>/wp-content/themes/bollywoodvideo/css/images/twitter_32.png"  title="Share on twitter"/></a>
				 
				 <a target="_blank" href="https://plus.google.com/share?url="><img src="<?php echo site_url()?>/wp-content/themes/bollywoodvideo/css/images/googleplus_32.png"  title="Share on google+"/></a>
				 
				 <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source="><img src="<?php echo site_url()?>/wp-content/themes/bollywoodvideo/css/images/linkedin_32.png"  title="Share on linkedin"/></a>
				 
				 <a class="whatsapp" data-link="<?php echo get_permalink() ?>" data-text="<?php echo get_the_title() ?>"><img src="<?php echo site_url()?>/wp-content/themes/bollywoodvideo/images/whatsapp.png"  title="Share on whatsup" /></a>
				  
				  </div>
	          </div>	          
			  <div class="col-xs-12 col-ms-6    visible-xs">
				  <p  style="margin: 8px 0;">Views : <?php echo get_post_meta(get_the_ID(),'total_view',true); ?> </p>
				  <div class="clearfix" style="margin-top:10px"></div>
				  <div>
				  Share on :<br> 
				 <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u="><img src="<?php echo site_url()?>/wp-content/themes/bollywoodvideo/css/images/facebook_32.png"  title="Share on facebook"/></a>
				 
				 <a target="_blank" href="https://twitter.com/home?status="><img src="<?php echo site_url()?>/wp-content/themes/bollywoodvideo/css/images/twitter_32.png"  title="Share on twitter"/></a>
				 
				 <a target="_blank" href="https://plus.google.com/share?url="><img src="<?php echo site_url()?>/wp-content/themes/bollywoodvideo/css/images/googleplus_32.png"  title="Share on google+"/></a>
				 
				 <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=&title=&summary=&source="><img src="<?php echo site_url()?>/wp-content/themes/bollywoodvideo/css/images/linkedin_32.png"  title="Share on linkedin"/></a>
				 
				 <a class="whatsapp" data-link="<?php echo get_permalink() ?>" data-text="<?php echo get_the_title() ?>"><img src="<?php echo site_url()?>/wp-content/themes/bollywoodvideo/images/whatsapp.png"  title="Share on whatsup" /></a>
				  
				  </div>
	          </div>
	          
	         
	           </div>
	        
	        
				<?php if($isvideo){?> 

				<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/jwplayer/jwplayer.js"></script>

				<script type="text/javascript">
				$(document).ready(function(){

					jwplayer("jwplayerload").setup({
						 
						
						file:'<?php echo $vidfile ?>'
						,
						fallback: true,
						androidhls: true,
						width: "100%",
						image: "<?php echo $image_attributes[0] ?>",
					});
				});    
				</script>
				<?php } ?>
				 
				<script type="text/javascript">
					
					function downfile(u){
						//alert('hi');
						//window.open(u, '_blank', 'location=yes');
						window.location.href=u;
					}
					
				jQuery(document).ready(function($) {
					
					
				var isMobile = {
					Android: function() {
						return navigator.userAgent.match(/Android/i);
					},
					BlackBerry: function() {
						return navigator.userAgent.match(/BlackBerry/i);
					},
					iOS: function() {
						return navigator.userAgent.match(/iPhone|iPad|iPod/i);
					},
					Opera: function() {
						return navigator.userAgent.match(/Opera Mini/i);
					},
					Windows: function() {
						return navigator.userAgent.match(/IEMobile/i);
					},
					any: function() {
						return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
					}
				};
				 $(document).on("click", '.whatsapp', function() {
						if( isMobile.any() ) {

							var text = $(this).attr("data-text");
							var url = $(this).attr("data-link");
							var message = encodeURIComponent(text) + " - " + encodeURIComponent(url);
							var whatsapp_url = "whatsapp://send?text=" + message;
							window.location.href = whatsapp_url;
						} else {
							alert("Please share this article in mobile device");
						}

					});
				});				
				
				</script>	        
	        
	        
	           
	     <?php		
					          
				endwhile;
			?>   
	    </div>
	        
	         <hr>
	           <div class="col-xs-12 col-sm-12 col-md-12 song-detail-blok">  
	           
				   <div id="search-song">
						<div class="title-main" style="border-bottom: 0;">
							<span class="title-b ">Related Video</span>
				   
						</div>
					</div>
					<?php
					
					$category = get_the_category();
					$scat=$category[0]->slug;
					if($scat){
						
						$args = array(
	 
							'posts_per_page'   => 8,
							'category_name' => $scat,
							'post_type'        => 'videos','orderby'=>'rand'

						);

						$related= get_posts( $args );
					}
					
					if(!$related || count($related)==0 || !($scat)){
						
						$args = array(
	 
							'posts_per_page'   => 8,
							
							'post_type'        => 'videos','orderby'=>'rand'

						);

						$related= get_posts( $args );						
						
						
					}		
					
				 
					?>
					 <?php foreach ( $related as $post ) : setup_postdata( $post ); ?>
					
					
					 
						<div class="col-xs-12 col-ms-4 col-sm-3 col-md-3 ">
						 
				 
								<div class="music-thumb">
									<a class="mainimg" href="<?php echo get_permalink() ?>"><?php echo get_the_post_thumbnail( get_the_ID(), array( 300, 152) );	?></a>
									 
									<a href="<?php echo get_permalink() ?>" title="<?php echo get_the_title() ?>" class="artwork_play"> <img src="<?php bloginfo('template_url'); ?>/images/play-ic.PNG" class="play"></a>
								</div>
								<div class="song-title">
									<div class="visible-ms visible-sm visible-md visible-lg"><div class="pull-left" style="margin: 0px;"><?php if(function_exists('the_ratings')) { the_ratings(); } ?></div><div class="pull-right" style="margin: 0px;"><p>Views :  <?php echo get_post_meta(get_the_ID(),'total_view',true); ?> </p></div><div class="clearfix"></div></div>									
									  <h4><?php echo get_the_title() ?></h4>
									  <div class="visible-xs"><?php if(function_exists('the_ratings')) { the_ratings(); } ?><p  style="margin: 8px 0;">Views :  <?php echo get_post_meta(get_the_ID(),'total_view',true); ?> </p><div class="clearfix"></div></div>									
										 
										
								</div>
						</div>					
				 
					
					<?php endforeach; wp_reset_postdata();?> 
	           
	           
	           </div>
	        
	        
	        
	     
</div>	



 <?php get_footer(); ?>

