<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'options_framework_theme'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Test data
	$test_array = array(
		'one' => __('One', 'options_framework_theme'),
		'two' => __('Two', 'options_framework_theme'),
		'three' => __('Three', 'options_framework_theme'),
		'four' => __('Four', 'options_framework_theme'),
		'five' => __('Five', 'options_framework_theme')
	);

	// Multicheck Array
	$multicheck_array = array(
		'one' => __('French Toast', 'options_framework_theme'),
		'two' => __('Pancake', 'options_framework_theme'),
		'three' => __('Omelette', 'options_framework_theme'),
		'four' => __('Crepe', 'options_framework_theme'),
		'five' => __('Waffle', 'options_framework_theme')
	);

	// Multicheck Defaults
	$multicheck_defaults = array(
		'one' => '1',
		'five' => '1'
	);

	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => '#bada55' );
		
	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}

	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/images/';

	$options = array();

	$options[] = array(
		'name' => __('Settings', 'options_framework_theme'),
		'type' => 'heading');
	
		$options[] = array(
		'name' => __('Header Logo', 'options_framework_theme'),
		'desc' => __('Upload Logo For Header!', 'options_framework_theme'),
		'id' => 'logo_header',
		'type' => 'upload');
		
		
		$options[] = array(
		'name' => __('Contact Number','options_framework_theme'),
		'desc' => __('Change Contact Number', 'options_framework_theme'),
		'id' => 'contact',
		'std' => '',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Email id','options_framework_theme'),
		'desc' => __('Change Email id', 'options_framework_theme'),
		'id' => 'emailId',
		'std' => '',
		'type' => 'text');
		
		$options[] = array(
		'name' => __('Banner Text', 'options_framework_theme'),
		'desc' => __('Enter banner text here.', 'options_framework_theme'),
		'id' => 'banner',
		'std' => '',
		'type' => 'textarea');
		
		
		$options[] = array(
		'name' => __('Home Title', 'options_framework_theme'),
		'desc' => __('Enter Home page title here.', 'options_framework_theme'),
		'id' => 'hometitle',
		'std' => '',
		'type' => 'textarea');
		
		
		$options[] = array(
		'name' => __('Home Page Text 1st', 'options_framework_theme'),
		'desc' => __('Enter first para of home page text.', 'options_framework_theme'),
		'id' => 'hometext1',
		'std' => '',
		'type' => 'textarea');
		
		$options[] = array(
		'name' => __('Home Page Text 2nd', 'options_framework_theme'),
		'desc' => __('Enter second para of home page text.', 'options_framework_theme'),
		'id' => 'hometext2',
		'std' => '',
		'type' => 'text');	

		$options[] = array(
		'name' => __('Linkedin', 'options_framework_theme'),
		'desc' => __('Enter linkedin url.', 'options_framework_theme'),
		'id' => 'linkedin',
		'std' => '',
		'type' => 'text');			
		
	
	   $options[] = array(
		'name' => __('Copyright Text', 'options_framework_theme'),
		'desc' => __('Enter Copyright Text.', 'options_framework_theme'),
		'id' => 'copyright',
		'std' => '',
		'type' => 'textarea');								
		
	 

	
	return $options;
}

/*
 * This is an example of how to add custom scripts to the options panel.
 * This example shows/hides an option when a checkbox is clicked.
 */

add_action('optionsframework_custom_scripts', 'optionsframework_custom_scripts');

function optionsframework_custom_scripts() { ?>

<script type="text/javascript">
jQuery(document).ready(function($) {

	$('#example_showhidden').click(function() {
  		$('#section-example_text_hidden').fadeToggle(400);
	});

	if ($('#example_showhidden:checked').val() !== undefined) {
		$('#section-example_text_hidden').show();
	}

});
</script>

<?php
}