<?php

get_header(); 

 
?>

	 
	 
	 
		 
		 
<?php


$args = array(
'orderby' => 'ID',
'order' => 'ASC',
'include' => '12,13,14,15,16,11'
);
$categories = get_categories($args);

 
foreach($categories as $cat){
?>


	      <div class="col-xs-12 col-sm-12 col-md-12">
			  
			  
		  <div class="title-main">
		      <span class="title-b"><?php echo $cat->cat_name ?> (<?php echo $cat->category_count ?>)</span>
			   <span class="title-red pull-right"><a href="<?php echo site_url().'/category/'. $cat->slug ?>">See All ></a></span>
		  </div>
		  <div class="clearfix"></div>			  
			  <?php
			  
			  $args = array(
	 
					'posts_per_page'   => 6,
					'category_name' => $cat->slug,
					'post_type'        => 'videos',

				);

				$banner= get_posts( $args );
			  
			  ?>
			  
			 <div class="flexslider crouscat carousel" id="cro<?php echo $cat->cat_ID ?>">
				 <ul class="slides" >
				 <?php foreach ( $banner as $post ) : setup_postdata( $post ); ?>
				  		<li>
						<a href="<?php echo get_permalink() ?>"><?php echo get_the_post_thumbnail( $post_id, array( 369, 369) );	?>
									   </a>
						 <label class="title"><?php echo get_the_title() ?></label>			   
						</li>
				  <?php endforeach; 
					wp_reset_postdata();?>
				 
				 
				  </ul>
			  </div>			  
		  </div>



<?php }  ?>


		  

	
<script>
jQuery( document ).ready(function($) {
  $('#mainslider').flexslider({
        animation: "slide"
	});
	
  $('.crouscat').flexslider({
    animation: "slide",
    
    itemWidth: 369,
    itemMargin: 18,
        minItems: 3,
    maxItems: 3
    
  });	
	
});

/*$(window).load(function() {
  $('#ff2').flexslider({
    animation: "slide",
    
    itemWidth: 210,
    itemMargin: 5,
    minItems: 3,
    maxItems: 3
  });
  
  $('#ff3').flexslider({
    animation: "slide",
    
    itemWidth: 210,
    itemMargin: 5,
    minItems: 3,
    maxItems: 3
  });
  
  $('#ff4').flexslider({
    animation: "slide",
    
    itemWidth: 210,
    itemMargin: 5,
    minItems: 3,
    maxItems: 3
  });
});*/

</script>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/flexslider.css">
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.flexslider-min.js"></script>

<?php get_footer(); ?>
