<?php
	if ( !function_exists( 'optionsframework_init' ) ) {
	define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/' );
	require_once dirname( __FILE__ ) . '/inc/options-framework.php';
}
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu',      'miracle' ),
		
	) );

	
	

	
/*function register_my_menu() {
  register_nav_menu( 'primary', 'Primary Menu' );
  } */

if ( function_exists('register_sidebar') ) {
	register_sidebar(array(
		'name' => 'footer-widget',
		'id' => 'footer-widget',
		'description' => 'This area for footer Section only',	
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	));




	}
	


add_filter('widget_text', 'do_shortcode');
add_filter('the_content', 'do_shortcode', 10);


 add_action( 'wp_ajax_searchresult', 'mvp_searchresult' ); // users.php
add_action( 'wp_ajax_nopriv_searchresult', 'mvp_searchresult' ); // users.php


function mvp_searchresult(){
  $result=array();	
 if(isset($_GET['term']) && !empty($_GET['term'])){
	 
	 global $wp_query;
		$search = $_GET['term'];
	  $args = array(
		's' => $search,
		'posts_per_page' => -1, 'post_type'=>'videos','meta_key'			=> 'isvalid',
					'orderby'			=> 'meta_value_num',
					'order'				=> 'DESC', 
	  );
	 $loop = new WP_Query( $args );
	// print_r($loop);
	 $i=0;
	 while ( $loop->have_posts() ) : $loop->the_post(); 
	   $result[$i]['name']=get_the_title();
	   $result[$i]['link']=get_the_permalink();
	   $result[$i]['view']=get_post_meta(get_the_ID(),'total_view',true);
	   ob_start();
	   the_ratings();
	   $htnl=ob_get_clean();
	   
	   $result[$i]['rating']=$htnl;
	   $result[$i++]['img']=get_the_post_thumbnail( get_the_ID(), array( 300, 152) );
	 endwhile;
	} 	
	echo json_encode($result);exit();
}	
