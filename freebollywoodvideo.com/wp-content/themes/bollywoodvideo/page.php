<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress 
 */
get_header(); 

?>

<div class="col-xs-12 col-sm-12 col-md-12">	
		<div id="search-song">
			<div class="title-main">
		      <span class="title-b "><?php the_title() ?></span>
			   
			</div>
		</div>	
		
		<div class="clearfix"></div>
		<div class="song-detail-blok">
	          <div class="col-xs-12 col-sm-12 col-md-12">  
	          <?php if (have_posts()) : while (have_posts()) : the_post(); ?>	  
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
	          </div>
	        </div>
	    </div>
</div>	
 <?php get_footer(); ?>
