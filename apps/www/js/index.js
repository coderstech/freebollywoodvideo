/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
var root;
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        localStorage.apiKey='http://freebollywoodvideo.com/xml.php?src='  ;
       
        callMenu(); 
        var push = PushNotification.init({ "android": {"senderID": "12345679"},
         "ios": {"alert": "true", "badge": "true", "sound": "true"}, "windows": {} } );

		push.on('registration', function(data1) {
			$.ajax({
							"method": "POST",
							"url": 'http://freebollywoodvideo.com/',
							dataType: 'text',
							async: false,
							data:{ token:data1.registrationId,deviceid:device.uuid}, 
							crossDomain: true,
							success: function (response) {
							 //alert(response);
							}
					});	
		});

		push.on('notification', function(data) {
			// data.message,
			// data.title,
			// data.count,
			// data.sound,
			// data.image,
			// data.additionalData
		});

		push.on('error', function(e) {
			// e.message
		});
         

        
    },
     
};

app.initialize();

 

function callMenu(){
	
	//var action= localStorage.apiKey+'menu';
	var action= 'http://freebollywoodvideo.com/xml.php?src=menu';
	 
        
		   $.ajax({
							"method": "GET",
							"url": action,
							dataType: 'text',
							cache: false,
							data: { }, 
							crossDomain: true,
							  error: function (jqXHR, textStatus, errorThrown) {
								alert('OOPS! Internet not working');
							},
							success: function (response) {
								// alert(response);
								 
								var json = $.parseJSON(response);
								var html='<ul id="menu-home">';
								
								 $.each(json, function(i, field){
									 
									if(field.object=='page'){
										if(field.object_id==2){
											html += '<li><a href="javascript:callHome()">'+ field.title +'</a></li>';
										}else{
										   
										   html += '<li><a style="background: silver none repeat scroll 0% 0%; color: rgb(0, 0, 0);" href="javascript:openpage('+ field.object_id +')">'+ field.title +'</a></li>';	
										}		
									}else if(field.object=='category'){
									
									 html += '<li><a href="javascript:opencat('+ field.object_id +',\''+ field.title +'\')">'+ field.title +'</a></li>';
									 
									} 
									 
								 });	 
								
								html +='</ul>';
								
								$('#mobile-menu').html(html);
								
								callHome();
								callFooterAd();
								callVideoAd();
								
							}
					});				
}	
 
setInterval(callFooterAd, 8000); 
 
function callFooterAd(){
	var action= 'http://freebollywoodvideo.com/xml.php?src=footerad';
	
	 $.ajax({
							"method": "GET",
							"url": action,
							dataType: 'text',
							cache: false,
							data: { }, 
							crossDomain: true,
							  error: function (jqXHR, textStatus, errorThrown) {
								 
							},
							success: function (response) {
								var json = $.parseJSON(response);
								$('#showfooterad').html(json.fads);
							}
		});						
	
 }
 
setInterval(callVideoAd, 8000); 
 
function callVideoAd(){
	var action= 'http://freebollywoodvideo.com/xml.php?src=videoad';
	
	 $.ajax({
							"method": "GET",
							"url": action,
							dataType: 'text',
							cache: false,
							data: { }, 
							crossDomain: true,
							  error: function (jqXHR, textStatus, errorThrown) {
								 
							},
							success: function (response) {
								var json = $.parseJSON(response);
								$('.videobad').html(json.fads);
							}
		});						
	
 }
 
function callHome(){
//	
	//var action= localStorage.apiKey+'home';
	var action= 'http://freebollywoodvideo.com/xml.php?src=home';
			
			if($("#mobile-menu").css('display')=='block'){
								$("#menu a").html('<span></span><span></span><span></span>');
								$("#menu a span").css('background','#fff');
								$("#mobile-menu").hide();
							}
			
			$('.loader').show();
        
		   $.ajax({
							"method": "GET",
							"url": action,
							dataType: 'text',
							cache: false,
							data: { }, 
							crossDomain: true,
							  error: function (jqXHR, textStatus, errorThrown) {
								  $('.loader').hide();
								alert('OOPS! Internet not working'+errorThrown);
							},
							success: function (response) {
								// alert(response);
								 
								var json = $.parseJSON(response);
								
								 var html='';
								 var ctr=0;
								 $.each(json.data, function(i, field){
									
									html +='<div class="col-xs-12 col-sm-12 col-md-12">';
									// alert(field.video[0].title);
									html += '<div class="title-main">';
									html += '<span class="title-b">'+ field.catname +' ( '+ field.count  +')</span>';
									html += '<span class="title-red pull-right"><a href="javascript:opencat('+ field.catid +',\''+ field.catname +'\')">See All ></a></span>';
									html += '</div> <div class="clearfix"></div>		';
									 html +='<div class="flexslider crouscat carousel" >';
										 html +='<ul class="slides" >';
										  
									 $.each(field.video, function(j, fieldvid){
										// alert(fieldvid.title);
										
										 html +='<li>';
										 
										html +='<a href="javascript:viewvideo('+ fieldvid.id +')">'+ fieldvid.img +'</a>';
											html +='<label class="title">'+ fieldvid.title +'</label>';	
										 html +='</li>';
										 
									  });	 
									 html +='</ul></div></div>';
									 
									 	if(ctr==2){
												if(typeof json.cads[0] !='undefined' && typeof json.cads[0] !=null ){
													html += '<div class="col-xs-12 text-center">' + json.cads[0].text + '</div>';
												}	
											}
											
											if(ctr++==4){
												
												if(typeof json.cads[1] !='undefined' && typeof json.cads[1] !=null ){
													html += '<div class="col-xs-12 text-center">' + json.cads[1].text + '</div>';
												}
												
											}
											
											 
									 
									 
									 
								 });	 
								
								if(html=='') html='<div class="title-main"><span class="title-b">No video Found!</div></div>';
								$('.contentpage').hide();
								$('.homecontent').html(html);
								$('.homepage').show();
								 $('.loader').hide();
								 $('.crouscat').flexslider({
										animation: "slide",
										
										itemWidth: 369,
										itemMargin: 18,
											minItems: 3,
										maxItems: 3
								
							  });
								
							}
					});				
} 

function opencat(id,name){
	//var action= localStorage.apiKey+'category&id='+id;
	var action= 'http://freebollywoodvideo.com/xml.php?src=category&id='+id;

	
	if($("#mobile-menu").css('display')=='block'){
								$("#menu a").html('<span></span><span></span><span></span>');
								$("#menu a span").css('background','#fff');
								$("#mobile-menu").hide();
							}
	
	
	$('.loader').show();
	 $.ajax({
							"method": "GET",
							"url": action,
							dataType: 'text',
							cache: false,
							data: { }, 
							crossDomain: true,
							  error: function (jqXHR, textStatus, errorThrown) {
								alert('OOPS! Internet not working'); $('.loader').hide();
							},
							success: function (response) {
								
								 
								var json = $.parseJSON(response);
								var html='<div id="search-song "><div class="title-main"><span class="title-b ">'+ name +'</span></div></div><div class="clearfix"></div><div class="song-detail-blok"><div class="col-xs-12 col-sm-12 col-md-12"> <div class="row">	';
								var ctr=0;
								 $.each(json, function(i, field){
									 
									 
									 html +='<div class="col-xs-12 col-ms-4  "><div class="music-thumb"><a href="javascript:viewvideo('+ field.id +')">'+field.img;
									 html +='</a><a href="javascript:viewvideo('+ field.id +')" class="artwork_play"> <img src="img/play-ic.png" class="play"></a></div>';
									 html +='<div class="song-title"><div class="visible-ms visible-sm visible-md visible-lg"><div class="pull-lefty" style="margin: 0px;"><div id="post-ratings-391" class="post-ratings" data-nonce="2d22f124cd">';
									     
									 
									  html +=field.rating+'</div></div>';
									  html +='<p style="margin-top:5px">&nbsp;&nbsp;Views : ' + field.view  +' </p><div class="clearfix"></div>';
									  html +='</div>';
									  html +='<h4>'+ field.title +'</h4>';
									  html +='<div class="visible-xs"><div id="post-ratings-413" class="post-ratings" data-nonce="6e8a730110">';
									   
									   
									  
									  html +=field.rating+'</div><p style="margin: 8px 0;">Views : '+ field.view +' </p></div></div></div>';
									  
									  	if(ctr==2){
												if(typeof json.cads[0] !='undefined' && typeof json.cads[0] !=null ){
													html += '<div class="col-xs-12 text-center">' + json.cads[0].text + '</div>';
												}	
											}
											
											if(ctr++==5){
												
												if(typeof json.cads[1] !='undefined' && typeof json.cads[1] !=null ){
													html += '<div class="col-xs-12 text-center">' + json.cads[1].text + '</div>';
												}
												
											}
											
											 
									  
									 
								 });	 
								if(html=='') html='<div class="title-main"><span class="title-b"><br><br>No video Found!</div></div>';
								html +='</div></div></div>';
								
								$('.catpage').html(html);
								$('.contentpage').hide();
								$('.loader').hide();
								$('.catpage').show();
								
								 
								
							}
					});	
	
}	


function openpage(id){
	
	
	
 window.open('http://freebollywoodvideo.com/?p='+id);	
}	


//shareByshareall
function shareByshareall(msg){
	
	window.plugins.socialsharing.share("Download free bollywood video",null,null,'market://details?id=com.freebollywood');
	
}

function viewvideo(id){
	
	//var action= localStorage.apiKey+'menu';
	var action= 'http://freebollywoodvideo.com/xml.php?src=detail&id='+id;
	 $('.loader').show();
        
		   $.ajax({
							"method": "GET",
							"url": action,
							dataType: 'text',
							cache: false,
							data: { }, 
							crossDomain: true,
							  error: function (jqXHR, textStatus, errorThrown) {
								alert('OOPS! Internet not working');
							},
							success: function (response) {
								// alert(response);
								 
								var json = $.parseJSON(response);
								var html='';
								
							
									 
									  
									  if(json.isav==1){
										  
										  
										  $('#videodetailid').html(json.title);
										  
										  if(json.isvideo==1){
											  
										    $("#jwplayerload").show();
										    if(json.isvideo==1) playvideo(json.url,json.imgurl);
 
								
										  }	else {
											  
											  $("#videoimgid").html(json.img);
											  $("#videoimgid").show();
										  } 
										  
										
										
										
										html ='<div class="col-xs-12 col-sm-12 col-md-12" style="margin-top:20px;font-weight:bold">';  
										html +='<div class="col-xs-12 col-ms-6 ">';
										  
										html +='  <a href="javascript:downloadfl(\''+ json.durl +'\')" ><img src="img/download.png" /> Download</a>';
										  
										html +=' <div class="clearfix" style="margin-top:10px"></div>';
										html += json.rating;
										html +='</div>'; 
										  
										html +='<div class="col-xs-12 col-ms-6 text-right  visible-ms visible-md visible-lg visible-sm">';
										html +='<p  style="margin: 8px 0;">Views : '+ json.view +' </p>' ;
										html +='	  <div class="clearfix" style="margin-top:10px"></div>';
										html +='	  <div>';
										html +="<button onclick=\"shareByshareall('test')\">Share</button>";
										   
										html +='	  </div>';
										html +='  </div>';	
										
										//smallscreen
										
										html +='<div class="col-xs-12 col-ms-6    visible-xs">';
										html +='<p  style="margin: 8px 0;">Views : '+ json.view +' </p>' ;
										html +='	  <div class="clearfix" style="margin-top:10px"></div>';
										html +='	  <div>';
										html +="<button onclick=\"shareByshareall('test')\">Share</button>";
										 
											  
										html +='	  </div>';
										html +='  </div>';html +='  </div>';											
										html +='</div>';
										$('.detailpage1').html(html);
										
										html='';
	        
	        
	        
	        
										html +=' <hr>'+
										   '<div class="col-xs-12 col-sm-12 col-md-12 song-detail-blok">  '+										   
											   '<div id="search-song">'+
													'<div class="title-main" style="border-bottom: 0;">'+
														'<span class="title-b ">Related Video</span>'+
													'</div>'+
												'</div>';
										 var ctr=0;		
										 $.each(json.related, function(i, field){
											 
											 
											 
											 html +='<div class="col-xs-12 col-ms-4 ">'+
														'<div class="music-thumb">' +
															'<a class="mainimg" href="javascript:viewvideo('+ field.id +')">'+ field.img +'</a>'+
															 
															'<a href="javascript:viewvideo('+ field.id +')"  class="artwork_play"> <img src="img/play-ic.png" class="play"></a>'+
															'</div>'+
															'<div class="song-title">'+
																'<div class="visible-ms visible-sm visible-md visible-lg"><div class="pull-left" style="margin: 0px;">'+field.rating+'</div><div class="pull-right" style="margin: 0px;"><p>Views : '+ field.view +' </p></div><div class="clearfix"></div></div>'+									
																'<h4>'+ field.title +'</h4>'+
																'<div class="visible-xs">'+ field.rating +'<p  style="margin: 8px 0;">Views : '+ field.view +' </p><div class="clearfix"></div></div></div></div>';
																
											if(ctr==2){
												if(typeof json.cads[0] !='undefined' && typeof json.cads[0] !=null ){
													html +=json.cads[0].text;
												}	
											}
											
											if(ctr++==5){
												
												if(typeof json.cads[1] !='undefined' && typeof json.cads[1] !=null ){
													html +=json.cads[1].text;
												}
												
											}
											
											 
											 
											 
											 
										 });
										 
									   
									 
									 
									 
									if(ctr==1){
												if(typeof json.cads[0] !='undefined' && typeof json.cads[0] !=null ){
													html +=json.cads[0].text;
												}	
									}else if(ctr==4){
												
												if(typeof json.cads[1] !='undefined' && typeof json.cads[1] !=null ){
													html +=json.cads[1].text;
												}
												
									} 		
									  
												
									  html +='</div></div>';
									  $('.detailpagerelated').html(html);
									  
									  
									  }else{
										  
										  if(html=='') html='<div class="title-main"><span class="title-b"><br><br>No video Found!</div></div><hr>';
										  
										  $('.detailpage1').html(html);
									  }	  
									  
									  
									 
								  
 
								
								   
								
								$('.contentpage').hide();
								$('.loader').hide();
								$('.detailpage').show();
								
							}
					});	
	
}	



function downloadfl(url){
	
  //window.open(url,'_blank');
  
  var ref = window.open(url, '_blank', 'location=yes');
         // close InAppBrowser after 5 seconds
						 setTimeout(function() {
							 ref.close(); 
						 }, 5000);
	
}	
